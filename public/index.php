<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';
require_once __DIR__.'/../config.php';
$settings = require __DIR__ . '/../bootstrap/settings.php';
$app = new Slim\App($settings);

require __DIR__ . '/../bootstrap/dependencies.php';

require __DIR__ . '/../bootstrap/middleware.php';

require __DIR__ . '/../bootstrap/controllers.php';

$app->run();

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Moe;
class Fichiers extends Model
{
    protected $table = 'fichiers';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'chemin_fichier', 'moe_id'];

    public $timestamps = false;

    public function Moe()
    {
        return $this->belongsTo('App\Models\Moe');
    }

    public function add_fichier($moe_id,$file_name)
    {
        $fichier = new Fichiers( ['chemin_fichier' => $file_name] );
        $moe = Moe::find($moe_id);
        return $moe->fichiers()->save($fichier);

    }

}
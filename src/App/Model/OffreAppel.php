<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class OffreAppel extends Model
{
    protected $table = 'appel_offre';
    public $timestamps = true;
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable=[

        'name',
        'dateDebut',
        'dateFin',
        'description',
        'reference',
        'profil',
        'id',
        'image_link'
    ];

}
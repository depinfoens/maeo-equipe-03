<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Moe extends Model
{
    protected $table = 'MOE';

    protected $primaryKey = 'id';


    public $timestamps = false;

    protected $fillable=[

        'nom',
        'prenom',
        'email',
        'fonction',
        'service',
        'telephone',
        'user_id',
        'id'
    ];

    public function addMoe($credentials) {

        return Moe::create($credentials);
    }


    public function Fichiers()
    {
        return $this->hasMany('App\Model\Fichiers');
    }


}
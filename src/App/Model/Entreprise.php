<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $table = 'entreprise';

    protected $primaryKey = 'id';



    public $timestamps = false;

    protected $fillable=[

        'libelle',
        'complement',
        'pays',
        'adresse',
        'code_postal',
        'ville',
        'telephone',
        'site_internet',
        'secteur_activite',
        'code_naf',
        'code_ape',
        'code_siret',
        'moe_id',
        'id'
    ];

    public function addEntreprise($credentials) {

        return Entreprise::create($credentials);
    }

}
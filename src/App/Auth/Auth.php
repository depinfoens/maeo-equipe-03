<?php

namespace  App\Auth;
use App\Model\User;
class Auth
{

    public function user()
    {

        return User::find($_SESSION['user']);
    }

    public function check()
    {

        return isset($_SESSION['user']);
    }

    public function attempt($username, $password)
    {
        //grab the user by email
        $user = User::where('username',$username)->first();

        if(!$user) {
            return false;
        }
        if(password_verify($password, $user->password)){

            $_SESSION['user'] = $user->id;

            return true;
        }

        return false;
    }


    public function logout()
    {
        unset($_SESSION['user']);


    }





}
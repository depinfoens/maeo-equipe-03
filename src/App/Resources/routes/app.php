<?php
use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;

$app->get('/', 'AppController:home')->setName('home');
$app->get('/search', 'AppController:search')->setName('search');

$app->get('/offre', 'OfferController:offre')->setName('offre');

$app->group('', function () {
    $this->get('/contract/create', 'AppController:getCreateContract')->setName('create.contract');
    $this->post('/contract/create', 'AppController:createContract')->setName('publish.contract');
})->add(new App\Middleware\AuthMiddleware($container));

<?php
use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;


$app->group('', function(){
    $this->map(['GET', 'POST'],'/moe/add','MoeController:getMoeForm')->setName('moe_form_identite');
    $this->post('/registerMoe/identite', 'MoeController:registerMoeIdentite')->setName('registerMoe.identite');
    $this->map(['GET', 'POST'],'/registerMoe/entreprise', 'MoeController:getMeoEntrepriseForm')->setName('moe_form_entreprise');
    $this->post('/registerMoe/entreprise/add', 'MoeController:registerMoeEntreprise')->setName('registerMoe.entreprise');
//  add other routes wich need that user signed in
})->add(new AuthMiddleware($container));
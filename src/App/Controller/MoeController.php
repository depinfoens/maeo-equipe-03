<?php

namespace App\Controller;

use App\Model\Fichiers;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
use App\Model\Moe;
use App\Model\Entreprise;
class MoeController extends Controller
{

    public function getMoeForm($request, $response)
    {

        return $this->view->render($response,'MOE/registermoeIdentite.twig',[

            "nom_form" => 'Création MOE',
            "title"=>"Création MOE",
            "action"=>$this->router->pathFor("registerMoe.identite"),
            "method"=>"post",
            "user_id"=> $_SESSION['user']
        ]);

    }







    public function registerMoeIdentite(Request $request, Response $response)
    {
        $posts =  $request->getParams();

        $moe = new Moe();
        $fichier = new Fichiers();
            $user_id = $_SESSION['user'];


            $validation = $this->validator->validate($request,[
                'nom'            => v::notEmpty()->length(1,45),
                'prenom'         => v::notEmpty()->length(1,45),
                'email'          => v::notEmpty()->length(1,45),
                'fonction'       => v::notEmpty()->length(1,45),
                'service'       => v::notEmpty()->length(1,45),
                'telephone'       => v::notEmpty()->length(1,45),
            ]);

           /*if( $validation->getErrors() ){
                $this->flash->addMessage('error','Il y a des erreurs dans le formulaire !');;
                return $response->withRedirect($this->router->pathFor('registerMoe.identite'));
            }*/
        // Setting up upload configuration and uploading the file !
        global $config;
        $upload_status = $this->upload('fichiers',$config['upload_dir'],'5M',array('image/png', 'image/gif' , 'image/jpeg','image/jpg','application/pdf' ) );
        echo "<pre>";
        print_r($upload_status);
        exit();

        if ( !empty($upload_status['error']) )
        {
            $this->flash->addMessage('error','Fichier non valide');
            return $response->withRedirect($this->router->pathFor('registerMoe.identite'));
        }
            $uuid = uniqid();

            $nom = $request->getParam('nom');
            $prenom = $request->getParam('prenom');
            $email = strip_tags($posts['email']);
            $fonction = $request->getParam('fonction');
            $service = $request->getParam('service');
            $telephone = strip_tags($posts['telephone']);

            $credentials = [
                'nom'               => $nom,
                'prenom'            => $prenom,
                'email'             => $email,
                'fonction'          => $fonction,
                'service'           => $service,
                'telephone'         => $telephone,
                'user_id'           => $user_id,
                'id'                => $uuid
            ];
            // Call the orgnaisateur Model and Register the user or fail

            $add_moe = $moe->addMoe($credentials)->id;

            if (  $add_moe === null )
            {
                $this->flash->addMessage('error','Une erreur dans la Base de donnée !');
                return $response->withRedirect($this->router->pathFor('registerMoe.identite'));
            }
            else
            {
                $fichier->add_fichier( $add_moe , $upload_status['name'] ) ;
                $this->flash->addMessage('success','Votre identité a bien été enregistrée !');
            }


            return $response->withRedirect($this->router->pathFor('moe_form_entreprise')) ;













    }
    public function  getMeoEntrepriseForm(Request $request, Response $response) {

        return $this->view->render($response,'MOE/registermoeEntreprise.twig',[

            "nom_form" => 'Création MOE',
            "title"=>"Création MOE",
            "action"=>$this->router->pathFor("registerMoe.entreprise"),
            "method"=>"post",
            "user_id"=> $_SESSION['user']
        ]);


    }

    public function registerMoeEntreprise(Request $request, Response $response)
    {
        //enregistrement entreprise ici

        $posts =  $request->getParams();

        $entrepise = new Entreprise();
        $moe = $_SESSION['user'];


        $validation = $this->validator->validate($request,[
            'libelle'            => v::notEmpty()->length(1,45),
            'complement'         => v::notEmpty()->length(1,45),
            'pays'         => v::notEmpty()->length(1,45),
            'adresse'          => v::notEmpty()->length(1,45),
            'code_postal'       => v::notEmpty()->length(1,45),
            'ville'       => v::notEmpty()->length(1,45),
            'site_internet'       => v::notEmpty()->length(1,45),
            'secteur_activite'       => v::notEmpty()->length(1,45),
            'code_naf'       => v::notEmpty()->length(1,45),
            'code_ape'       => v::notEmpty()->length(1,45),
            'code_siret'       => v::notEmpty()->length(1,45),
            'moe_id'       => v::notEmpty()->length(1,45)
        ]);

        /*if( $validation->getErrors() ){
             $this->flash->addMessage('error','Il y a des erreurs dans le formulaire !');;
             return $response->withRedirect($this->router->pathFor('registerMoe.identite'));
         }*/


        $libelle = $request->getParam('libelle');
        $complement = $request->getParam('complement');
        $pays = $request->getParam('pays');
        $adresse = strip_tags($posts['adresse']);
        $code_postal = $request->getParam('code_postal');
        $ville = $request->getParam('ville');
        $telephone = $request->getParam('telephone');
        $site_internet= $request->getParam('site_internet');
        $secteur_activite = strip_tags($posts['secteur_activite']);
        $code_naf = strip_tags($posts['code_naf']);
        $code_ape = strip_tags($posts['code_ape']);
        $code_siret = strip_tags($posts['code_siret']);
        $moe_id = $moe;

        $credentials = [
            'id'                  => uniqid(),
            'libelle'             => $libelle,
            'complement'          => $complement,
            'pays'                => $pays,
            'adresse'             => $adresse ,
            'code_postal'         => $code_postal,
            'ville'               => $ville,
            'telephone'           => $telephone,
            'site_internet'       => $site_internet,
            'secteur_activite'    => $secteur_activite,
            'code_naf'            => $code_naf,
            'code_ape'            => $code_ape,
            'code_siret'          => $code_siret,
            'moe_id'              => $moe_id
        ];
        // Call the orgnaisateur Model and Register the user or fail

        $add_entreprise = $entrepise->addEntreprise($credentials)->id;
        if (  $add_entreprise === null )
        {
            $this->flash->addMessage('error','Une erreur dans la Base de donnée !');
            return $response->withRedirect($this->router->pathFor('moe_form_entreprise'));
        }
        else
        {

            $this->flash->addMessage('success','Votre identité a bien été enregistrée !');
        }


        return $response->withRedirect($this->router->pathFor('home')) ;



    }
}
<?php

namespace App\Controller;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as V;
use App\Model\OffreAppel;
use Awurth\Upload\File;
use Awurth\Upload\Validation\Size;
use Awurth\Upload\Validation\MimeType;

use App\Model\Moe;
use App\Model\Entreprise;

class AppController extends Controller
{
    public function home(Request $request, Response $response){
        $res = [];
        $c = OffreAppel::count();
        $listeOffer;
        if($c<=5){
            $listeOffer = OffreAppel::all();
        }else{
            $listeOffer = OffreAppel::orderBy('dateDebut', 'ASC')->take(5)->get();
        }

        $res['offres'] = $listeOffer;
        return $this->view->render($response, 'App/home.twig', $res);
    }

    public function getCreateContract(Request $request, Response $response)
    {
        return $this->view->render($response,'AppelOffre/create_contract.twig');
    }

    public function search(Request $request, Response $response)
    {
        $query = $request->getParam('search-query');
        $res = [];

        if (!$query) {
            return $this->view->render($response, 'Error/error.twig', [
                'title' => 'Recherche vide',
                'description' => 'Veuillez renseigner des mots clés pour lancer la recherche'
            ]);
        }

        $appelsoffre = OffreAppel::where('name', 'like', '%' . $query . '%')
            ->get()->toArray();


        return $this->view->render($response, 'App/search.twig', [
            'offres' => $appelsoffre,
            'query' => $query
        ]);
    }

    public function createContract(Request $request, Response $response) {

         $validation = $this->validator->validate($request,[
            'name'            => V::notEmpty()->length(1,100),
            'dateDebut'         => V::notEmpty()->date('Y-m-d'),
            'dateFin'          => V::notEmpty()->date('Y-m-d'),
            'description'       => V::notEmpty()->length(1,1000),
            'reference'       => V::notEmpty()->length(1,100),
            'profil'       => V::notEmpty()->length(1,100),
        ]);

        if($validation->getErrors() ){
             $this->flash->addMessage('error','Il y a des erreurs dans le formulaire !');
             return $response->withRedirect($this->router->pathFor('home'));
        }
        else {
            $uuid = uniqid();

            $file = new File('image_link', 'images');
            $file->setNewName('image_'.$uuid);

        	$ext = $file->getExtension();


            $file->addConstraints([
                new Size('2M'),
                new MimeType(['image/png', 'image/jpeg'])
            ]);

            $offre = new OffreAppel([
                'name' => $request->getParam('name'),
                'dateDebut' => $request->getParam('dateDebut'),
                'dateFin' => $request->getParam('dateFin'),
                'description' => $request->getParam('description'),
                'reference' => $request->getParam('reference'),
                'profil' => $request->getParam('profil'),
                'image_link' => $file->getNewName().".".$ext,
                'id' => $uuid
            ]);

            $offre->save();

            if ($file->validate()->isValid()) {
                $file->upload();
            } else {
                $errors = $file->getErrors();
            }

            //A CHANGER QUAND L'AFFICHAGE DETAILLE D'UN APPEL D'OFFRE SERA FAIT
            return $response->withRedirect($this->router->pathFor('home'));
        }
    }
}
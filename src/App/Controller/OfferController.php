<?php

namespace App\Controller;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\Model\Moe;
use App\Model\Entreprise;
use App\Model\OffreAppel;
class OfferController extends Controller{
    public function offre(Request $request, Response $response, $args){
    	$res= $args;
   		if(!empty($_GET['id']) ){

      		$res['offre'] = OffreAppel::find($_GET['id']); 
      	}else{
      		$res['error'][] = 'aucune offre';
      	}

        return $this->view->render($response, 'AppelOffre/offre.twig', $res);
    }
}
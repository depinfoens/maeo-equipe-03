<?php

namespace App\Controller;

use Awurth\Slim\Validation\Validator;
use Cartalyst\Sentinel\Sentinel;
use Psr\Http\Message\ResponseInterface as Response;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\NotFoundException;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;

/**
 * @property Twig view
 * @property Router router
 * @property Messages flash
 * @property Validator validator
 * @property Sentinel auth
 */
class Controller
{
    /**
     * Slim application container
     *
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Redirect to route
     *
     * @param Response $response
     * @param string $route
     * @param array $params
     * @return Response
     */
    public function redirect(Response $response, $route, array $params = array())
    {
        return $response->withRedirect($this->router->pathFor($route, $params));
    }

    /**
     * Redirect to url
     *
     * @param Response $response
     * @param string $url
     *
     * @return Response
     */
    public function redirectTo(Response $response, $url)
    {
        return $response->withRedirect($url);
    }

    /**
     * Write JSON in the response body
     *
     * @param Response $response
     * @param mixed $data
     * @param int $status
     * @return int
     */
    public function json(Response $response, $data, $status = 200)
    {
        return $this->write($response, json_encode($data), $status);
    }

    /**
     * Write text in the response body
     *
     * @param Response $response
     * @param string $data
     * @param int $status
     * @return int
     */
    public function write(Response $response, $data, $status = 200)
    {
        return $response->withStatus($status)->getBody()->write($data);
    }

    /**
     * Add a flash message
     *
     * @param string $name
     * @param string $message
     */
    public function flash($name, $message)
    {
        $this->flash->addMessage($name, $message);
    }

    /**
     * Create new NotFoundException
     *
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return NotFoundException
     */
    public function notFoundException(ServerRequestInterface $request, Response $response)
    {
        return new NotFoundException($request, $response);
    }

    public function __get($property)
    {
        return $this->container->get($property);
    }



    //fonction pour ulpoad de fichiers (images, pdf...)

    public function upload($input_name,$upload_dir,$max_size,$allowed_types)
    {
        $storage = new \Upload\Storage\FileSystem($upload_dir);
        $file = new \Upload\File($input_name, $storage);

        // Generate unique file name
        $new_filename = uniqid();
        $file->setName($new_filename);

        $file->addValidations(array(
            // Ensure file is of desired type
            new \Upload\Validation\Mimetype($allowed_types),
            // Fix the max file size
            new \Upload\Validation\Size($max_size)
        ));

        // Try to upload files or fail
        try {

            $file->upload();
        }
        catch (\Exception $e) {
            $errors = $file->getErrors();
        }

        // Access data about the file that has been uploaded
        $data = array(
            'name'       => $file->getNameWithExtension(),
            'extension'  => $file->getExtension(),
            'mime'       => $file->getMimetype(),
            'size'       => $file->getSize(),
            'md5'        => $file->getMd5(),
            'dimensions' => $file->getDimensions(),
            'error' => $file->getErrors()
        );

        return $data;

    }
}

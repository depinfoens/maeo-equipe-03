-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 26 Janvier 2017 à 17:16
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `maeo`
--

-- --------------------------------------------------------

--
-- Structure de la table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'BS2xNtxSR2olScvJmvpit6eZWq2VzJWM', 1, '2017-01-13 13:14:59', '2017-01-13 13:14:59', '2017-01-13 13:14:59');

-- --------------------------------------------------------

--
-- Structure de la table `appel_offre`
--

CREATE TABLE `appel_offre` (
  `name` varchar(100) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `reference` varchar(200) NOT NULL,
  `profil` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `id` varchar(15) NOT NULL,
  `lieu` varchar(200) DEFAULT NULL,
  `image_link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `appel_offre`
--

INSERT INTO `appel_offre` (`name`, `dateDebut`, `dateFin`, `reference`, `profil`, `description`, `created_at`, `updated_at`, `id`, `lieu`, `image_link`) VALUES
('offre d\'appel', '2017-01-09', '2017-01-18', 'I\'m sorry I\'m not good in English. This is my code. I want to generate a new id value relevant to the hotel that customer select. I did it like this. Is there any better solution for this?\r\n\r\n', 'lkfjshdflkh lkjdshf \r\n', 'I\'m sorry I\'m not good in English. This is my code. I want to generate a new id value relevant to the hotel that customer select. I did it like this. Is there any better solution for this?\r\n\r\n', '2017-01-03', '2017-01-03', '582c4957abcd3', '15465 465464', NULL),
('fdgd', '2016-12-30', '2017-01-17', 'sg', 'fh', 'dsfg', '2017-01-25', '2017-01-25', '588901a9b5355', NULL, NULL),
('lolopop', '2017-01-19', '2017-02-03', 'sdfg', 'sdgs', 'qdsfhsgggggggggggggggggggggg gggggggggggggg fdddddd', '2017-01-25', '2017-01-25', '58890c3841ee9', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '58891072bcf2d', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '588911cf235f1', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '588913ab2fd1e', NULL, 'image_588913ab2fd1e.jpg'),
('jfg', '2017-01-10', '2017-01-20', 'fj', 'fgj', 'fg', '2017-01-31', '2017-01-25', 'hfjfgk', 'jfgh', 'fgj'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhdhsghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhdhslmghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhhsghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdghdf', 'sdg', 's');

-- --------------------------------------------------------

--
-- Structure de la table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'dkgSZOlw4Q03RcX6M7dAI509kEmZ8qUu', '2017-01-13 13:15:21', '2017-01-13 13:15:21'),
(2, 1, 'iaG0d7D0MEtgALQtkRfV2xBe38bMU3vf', '2017-01-25 18:31:11', '2017-01-25 18:31:11');

-- --------------------------------------------------------

--
-- Structure de la table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `permissions` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '{"user.create":true,"user.update":true,"user.delete":true}', '2017-01-13 12:50:08', '2017-01-13 12:50:08'),
(2, 'user', 'User', '{"user.update":true}', '2017-01-13 12:50:08', '2017-01-13 12:50:08');

-- --------------------------------------------------------

--
-- Structure de la table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2017-01-13 13:14:59', '2017-01-13 13:14:59');

-- --------------------------------------------------------

--
-- Structure de la table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2017-01-13 12:50:41', '2017-01-13 12:50:41'),
(2, NULL, 'ip', '::1', '2017-01-13 12:50:41', '2017-01-13 12:50:41'),
(3, NULL, 'global', NULL, '2017-01-25 18:30:41', '2017-01-25 18:30:41'),
(4, NULL, 'ip', '::1', '2017-01-25 18:30:41', '2017-01-25 18:30:41');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `permissions` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `last_name`, `first_name`, `permissions`, `created_at`, `updated_at`, `last_login`) VALUES
(1, 'lolipop', 'test@tets.fr', '$2y$10$rpQxW9enKcH3gZj2UN1Axem5RuXH.CkHEGLkpyD1qoqNfmyVuP8Ty', NULL, NULL, '{"user.delete":0}', '2017-01-13 13:14:59', '2017-01-25 18:31:11', '2017-01-25');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activations_user_id_foreign` (`user_id`);

--
-- Index pour la table `appel_offre`
--
ALTER TABLE `appel_offre`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`),
  ADD KEY `persistences_user_id_foreign` (`user_id`);

--
-- Index pour la table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reminders_user_id_foreign` (`user_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Index pour la table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`);

--
-- Index pour la table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_foreign` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_unique` (`username`),
  ADD UNIQUE KEY `user_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

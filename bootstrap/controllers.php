<?php

$container['AppController'] = function ($container) {
    return new App\Controller\AppController($container);
};

$container['AuthController'] = function ($container) {
    return new App\Controller\AuthController($container);
};


$container['MoeController'] = function ($container) {
    return new App\Controller\MoeController($container);
};

$container['OfferController'] = function($container) {
	return new App\Controller\OfferController($container);
};
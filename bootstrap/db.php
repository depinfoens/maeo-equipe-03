<?php

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'maeo-equipe-03',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => ''
];

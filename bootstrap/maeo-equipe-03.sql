-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 27, 2017 at 03:11 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maeo-equipe-03`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(4, 4, 'IfIPXTdql9bcFsKkufbjZCJqhW1NbaCm', 1, '2017-01-26 23:11:52', '2017-01-26 23:11:52', '2017-01-26 23:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `appel_offre`
--

CREATE TABLE `appel_offre` (
  `name` varchar(100) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `reference` varchar(200) NOT NULL,
  `profil` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `id` varchar(15) NOT NULL,
  `lieu` varchar(200) DEFAULT NULL,
  `image_link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appel_offre`
--

INSERT INTO `appel_offre` (`name`, `dateDebut`, `dateFin`, `reference`, `profil`, `description`, `created_at`, `updated_at`, `id`, `lieu`, `image_link`) VALUES
('offre d\'appel', '2017-01-09', '2017-01-18', 'I\'m sorry I\'m not good in English. This is my code. I want to generate a new id value relevant to the hotel that customer select. I did it like this. Is there any better solution for this?\r\n\r\n', 'lkfjshdflkh lkjdshf \r\n', 'I\'m sorry I\'m not good in English. This is my code. I want to generate a new id value relevant to the hotel that customer select. I did it like this. Is there any better solution for this?\r\n\r\n', '2017-01-03', '2017-01-03', '582c4957abcd3', '15465 465464', NULL),
('fdgd', '2016-12-30', '2017-01-17', 'sg', 'fh', 'dsfg', '2017-01-25', '2017-01-25', '588901a9b5355', NULL, NULL),
('lolopop', '2017-01-19', '2017-02-03', 'sdfg', 'sdgs', 'qdsfhsgggggggggggggggggggggg gggggggggggggg fdddddd', '2017-01-25', '2017-01-25', '58890c3841ee9', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '58891072bcf2d', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '588911cf235f1', NULL, NULL),
('fdhsddfj', '2017-01-05', '2017-01-26', 'fds', 'dsg', 'fgkhh', '2017-01-25', '2017-01-25', '588913ab2fd1e', NULL, 'image_588913ab2fd1e.jpg'),
('jfg', '2017-01-10', '2017-01-20', 'fj', 'fgj', 'fg', '2017-01-31', '2017-01-25', 'hfjfgk', 'jfgh', 'fgj'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhdhsghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhdhslmghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdfhhsghdf', 'sdg', 's'),
('test', '2017-01-21', '2017-01-12', 'fg', 'fhsd', 'sdfg', '2017-01-04', '2017-01-04', 'sdghdf', 'sdg', 's');

-- --------------------------------------------------------

--
-- Table structure for table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `libelle` varchar(256) NOT NULL,
  `complement` varchar(256) NOT NULL,
  `pays` varchar(256) NOT NULL,
  `adresse` varchar(256) NOT NULL,
  `code_postal` int(11) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `telephone` varchar(256) NOT NULL,
  `site_internet` varchar(50) DEFAULT NULL,
  `secteur_activite` varchar(256) NOT NULL,
  `code_naf` varchar(256) NOT NULL,
  `code_ape` varchar(256) NOT NULL,
  `code_siret` varchar(256) NOT NULL,
  `moe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fichiers`
--

CREATE TABLE `fichiers` (
  `id` int(11) NOT NULL,
  `chemin_fichier` varchar(255) NOT NULL,
  `moe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MOE`
--

CREATE TABLE `MOE` (
  `id` int(11) NOT NULL,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `fonction` varchar(256) NOT NULL,
  `service` varchar(256) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `permissions` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '{"user.create":true,"user.update":true,"user.delete":true}', '2017-01-26 22:39:37', '2017-01-26 22:39:37'),
(2, 'user', 'User', '{"user.update":true}', '2017-01-26 22:39:37', '2017-01-26 22:39:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(4, 2, '2017-01-26 23:11:52', '2017-01-26 23:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2017-01-26 22:53:09', '2017-01-26 22:53:09'),
(2, NULL, 'ip', '127.0.0.1', '2017-01-26 22:53:09', '2017-01-26 22:53:09'),
(3, NULL, 'global', NULL, '2017-01-26 22:53:19', '2017-01-26 22:53:19'),
(4, NULL, 'ip', '127.0.0.1', '2017-01-26 22:53:19', '2017-01-26 22:53:19'),
(5, NULL, 'global', NULL, '2017-01-26 22:53:55', '2017-01-26 22:53:55'),
(6, NULL, 'ip', '127.0.0.1', '2017-01-26 22:53:55', '2017-01-26 22:53:55'),
(7, NULL, 'global', NULL, '2017-01-26 22:54:04', '2017-01-26 22:54:04'),
(8, NULL, 'ip', '127.0.0.1', '2017-01-26 22:54:04', '2017-01-26 22:54:04'),
(9, NULL, 'global', NULL, '2017-01-26 22:56:50', '2017-01-26 22:56:50'),
(10, NULL, 'ip', '127.0.0.1', '2017-01-26 22:56:50', '2017-01-26 22:56:50'),
(11, NULL, 'global', NULL, '2017-01-26 22:57:01', '2017-01-26 22:57:01'),
(12, NULL, 'ip', '127.0.0.1', '2017-01-26 22:57:01', '2017-01-26 22:57:01'),
(13, NULL, 'global', NULL, '2017-01-26 23:09:02', '2017-01-26 23:09:02'),
(14, NULL, 'ip', '127.0.0.1', '2017-01-26 23:09:02', '2017-01-26 23:09:02'),
(15, NULL, 'global', NULL, '2017-01-26 23:09:23', '2017-01-26 23:09:23'),
(16, NULL, 'ip', '127.0.0.1', '2017-01-26 23:09:23', '2017-01-26 23:09:23'),
(17, NULL, 'global', NULL, '2017-01-27 00:34:36', '2017-01-27 00:34:36'),
(18, NULL, 'ip', '127.0.0.1', '2017-01-27 00:34:37', '2017-01-27 00:34:37'),
(19, NULL, 'global', NULL, '2017-01-27 00:35:25', '2017-01-27 00:35:25'),
(20, NULL, 'ip', '127.0.0.1', '2017-01-27 00:35:25', '2017-01-27 00:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `permissions` text NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `last_name`, `first_name`, `permissions`, `last_login`, `created_at`, `updated_at`) VALUES
(4, 'Kamel1979', 'remakikamel@hotmail.fr', '$2y$10$GJRbiUwBVC/EZOXYeKmi..IYbHOa86D54GXa0VJs9QTI9HkRX.49m', NULL, NULL, '{"user.delete":0}', '2017-01-26 23:12:08', '2017-01-26 23:11:52', '2017-01-26 23:12:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activations_user_id_foreign` (`user_id`);

--
-- Indexes for table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `moe_id` (`moe_id`);

--
-- Indexes for table `fichiers`
--
ALTER TABLE `fichiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `moe_id` (`moe_id`);

--
-- Indexes for table `MOE`
--
ALTER TABLE `MOE`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`),
  ADD KEY `persistences_user_id_foreign` (`user_id`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reminders_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_foreign` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_unique` (`username`),
  ADD UNIQUE KEY `user_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `fichiers`
--
ALTER TABLE `fichiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `MOE`
--
ALTER TABLE `MOE`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activations`
--
ALTER TABLE `activations`
  ADD CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `fichiers`
--
ALTER TABLE `fichiers`
  ADD CONSTRAINT `fichiers_to_moe` FOREIGN KEY (`moe_id`) REFERENCES `MOE` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `MOE`
--
ALTER TABLE `MOE`
  ADD CONSTRAINT `fk_moe_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `persistences`
--
ALTER TABLE `persistences`
  ADD CONSTRAINT `persistences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `reminders`
--
ALTER TABLE `reminders`
  ADD CONSTRAINT `reminders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `throttle_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
